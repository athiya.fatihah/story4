from django.shortcuts import render, redirect

from .forms import PostForm
from .models import Schedule

# Create your views here.
def kreasi1(request):
    return render(request, 'kreasi1.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def story(request):
    return render(request, 'story.html')

def schedule(request):
    return render(request, 'schedule.html')

def adventure(request):
    return render(request, 'adventure.html')


#####
def list(request):
    posts = Schedule.objects.all()

    context = {
        'page_title':'Semua Post',
        'posts':posts,
    }
    return render(request,'blog/list.html',context)

def create(request):
    post_form = PostForm(request.POST or None)

    if request.method == 'POST': # POST request dari browser
        if post_form.is_valid():
            post_form.save()

            return redirect('blog:list')

    context = {
        'page_title':'Create Post',
        'post_form':post_form,
    }
    return render(request,'blog/create.html',context)


