from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.kreasi1, name='kreasi1'),
    path('home/', views.kreasi1, name='kreasi1'),
    path('about-me/', views.aboutme, name='aboutme'),
    path('story/', views.story, name='story'),
    path('schedule/', views.schedule, name='schedule'),
    path('adventure/', views.adventure, name='adventure'),
    path('create/', views.create, name='create'),
    path('list/', views.list, name='list'),
]
